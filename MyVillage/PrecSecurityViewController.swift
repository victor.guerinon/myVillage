//
//  ViewPrecSecurityAlert.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 22/03/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class PrecSecurityViewController: UIViewController {
    
    
    @IBOutlet var nameLevelAlert: UILabel!
    @IBOutlet var imageLevelAlert: UIImageView!
    @IBOutlet var buttonCallView: UIView!
    
    var numberCall = UIButton()
    
    var typeAlert: TypeAlerte!
    var typeDanger: TypeDanger!
    
    var phoneNumber: Int = 0
    
    var callIsOk: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonWidth = CGFloat(140)
        let buttonHeight = CGFloat(80)
        
        let buttonX = buttonCallView.bounds.midX - (buttonWidth/2)
        let buttonY = buttonCallView.bounds.midY - (buttonHeight/2)
        
        numberCall.frame = CGRect(x: buttonX , y: buttonY , width: buttonWidth, height: buttonHeight)
        
        buttonCallView.addSubview(numberCall)
        
        numberCall.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callServicePressed)))
        
       switch typeDanger! {
        case .DANGER:
            imageLevelAlert.image = #imageLiteral(resourceName: "alarm-alert")
            nameLevelAlert.text = "Danger avéré"
        case .SOUPCON:
            imageLevelAlert.image = #imageLiteral(resourceName: "suspicious-alert")
            nameLevelAlert.text = "Soupçon"
        }
        
        
        switch typeAlert! {
        case .INCENDIE:
            numberCall.setBackgroundImage(#imageLiteral(resourceName: "call-fireman"), for: UIControlState.normal)
            phoneNumber = 18
        case .CAMBRIOLAGE:
            numberCall.setBackgroundImage(#imageLiteral(resourceName: "call-police"), for: UIControlState.normal)
            phoneNumber = 17
        case .DETRESSE:
            numberCall.setBackgroundImage(#imageLiteral(resourceName: "call-samu"), for: UIControlState.normal)
            phoneNumber = 15
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // sensé être fonctionnel mais ne fonctionne par sur simulateur d'après stack overflow
    func callServicePressed(){
        
        if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
            callIsOk = true
        }
    }

    
    func continueAlert(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LocationAlert") as! LocationViewController
        controller.typeDanger = typeDanger
        controller.typeAlert = typeAlert
        
        self.navigationController?.show(controller, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "continueSegue"{
            let levelAlertVC = segue.destination as? LocationViewController
            let typeAlertVC = segue.destination as? LocationViewController
            let callServiceVC = segue.destination as? LocationViewController
            levelAlertVC?.typeDanger = typeDanger
            typeAlertVC?.typeAlert = typeAlert
            callServiceVC?.callService = callIsOk
        

        }
        
    }

}


