//
//  ChoiceCommuneViewController.swift
//  MyVillage
//
//  Created by Victor Guerinon on 15/09/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit

class ChoiceCommuneViewController: UIViewController {
    var context: CtxtSubDistrict!
    var codePostal: String!
    var ville: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as? ChoiceDistrictViewController
        controller?.context = context
    }
    
    
    
}
