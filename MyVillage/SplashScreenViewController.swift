//
//  SplashScreenViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 24/07/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController{
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Voir si une connexion a déjà été enregistrée
        // Si oui chargement de l'acceuil
        // sinon chargement de la homePage
        
        // appel à needVersion
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // créer une fonction alerte pour le cas ou impossible de lancer l'app
    
    
}
