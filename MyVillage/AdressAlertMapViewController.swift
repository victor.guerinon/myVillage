//
//  AdressAlertMapViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 23/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit
import MapKit

class AdressAlertMapViewController: UIViewController{
    
    @IBOutlet var adressMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let location = CLLocationCoordinate2DMake(48.8726541, 2.7756662)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Le foyer impacte"
        
        let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: location, span: span)
        
        adressMap.setRegion(region, animated: true)
        
        adressMap.addAnnotation(annotation)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
