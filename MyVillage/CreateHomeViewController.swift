//
//  CreateHomeController.swift
//  MyVillage
//
//  Created by Victor Guerinon on 17/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit

class CreateHomeViewController : UIViewController {
    @IBOutlet var nmFamille: UITextField!
    
    var nomF: String?
    var prenomF: String?
    var mailF: String?
    var numeroF: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nomFoyer = segue.destination as? CreateAdressViewController
        let prenomFoyer = segue.destination as? CreateAdressViewController
        let mailFoyer = segue.destination as? CreateAdressViewController
        let numeroFoyer = segue.destination as? CreateAdressViewController
        let nomFamille = segue.destination as? CreateAdressViewController
        
        nomFoyer?.nomF = nomF
        prenomFoyer?.prenomF = prenomF
        mailFoyer?.mailF = mailF
        numeroFoyer?.numeroF = numeroF
        nomFamille?.nmFamille = nmFamille.text
        
    }
    
    
    
    
    
    
}
