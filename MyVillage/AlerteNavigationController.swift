//
//  AlerteNavigationController.swift
//  MyVillage
//
//  Created by Victor Guerinon on 15/09/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit

class AlerteNavigationController : UINavigationController {
    
    var typeAlert : TypeAlerte!
    
    override func viewDidLoad() {
        let vc = viewControllers[0] as! TypeAlertViewController
        vc.typeAlert = typeAlert
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = viewControllers[0] as! TypeAlertViewController
        vc.typeAlert = typeAlert
    }
}
