//
//  MyHomeViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 28/07/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class MyHomeViewController: UIViewController{
    
    @IBOutlet var idFoyer: UILabel!
    @IBOutlet var passwordFoyer: UILabel!
    @IBOutlet var nameFoyer: UILabel!
    @IBOutlet var adressFoyer: UILabel!
    @IBOutlet var nbNeighbor: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
