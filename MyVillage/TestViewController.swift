//
//  TestViewController.swift
//  MyVillage
//
//  Created by Damien Hervé on 29/09/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit
import MyVillageAPI

class TestViewController : UIViewController {
    override func viewDidLoad() {
        let credentials = Credentials()
        credentials.idFoyer = "E4AS3LTF"
        credentials.password = "306EA778"
        //Config.setAuthentication(credentials)
        ApiVersionAPI.apiVersionNeedUpdateGet(currentVersion: 1) { (version, error) in
            guard error == nil, let version = version else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
            print(version.needUpdate)
        }
    }
}

extension NSURLRequest {
    static func allowsAnyHTTPSCertificateForHost(host: String) -> Bool {
        return true
    }
}
