//
//  NeighborAlertViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 22/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class NeighborAlertViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBAction func closeClicked(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NeighborTableViewCell
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name:"FollowAlert", bundle:nil)
        let followAlert = storyboard.instantiateViewController(withIdentifier: "follow-alert") as! FollowAlertViewController
        
        self.navigationController?.pushViewController(followAlert, animated: true)
    }
}

