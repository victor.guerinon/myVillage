//
//  ViewTypeAlert.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 21/03/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class TypeAlertViewController: UIViewController {
    
    var typeAlert: TypeAlerte!
    var typeDanger: TypeDanger!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let levelAlertVC = segue.destination as! PrecSecurityViewController
        let typeAlertVC = segue.destination as! PrecSecurityViewController
        switch segue.identifier {
        case "dangerAlertSegue"?:
            typeDanger = .DANGER
        case "suspiciousAlertSegue"?:
            typeDanger = .SOUPCON
        default:
            break
        }
        levelAlertVC.typeDanger = typeDanger
        typeAlertVC.typeAlert = typeAlert
    }
}
