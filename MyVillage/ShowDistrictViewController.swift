//
//  ShowDistrictViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 09/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ShowDistrictViewController: UIViewController {

    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var districtMap: MKMapView!
    @IBOutlet var listView: UIView!
    @IBOutlet var mainButton: UIButton!
    
    var mapReady: Bool = false
    
    
    @IBAction func closeClicked(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        mainButton.setTitle("Afficher la liste de mon quartier", for: .normal)
        
        let currentWidthScreen = UIScreen.main.bounds.width
        
        scrollView.contentSize = CGSize(width: 3 * currentWidthScreen, height: 300)
        
        scrollView.isScrollEnabled = false
        
        let location = CLLocationCoordinate2DMake(48.8726541, 2.7756662)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Disney Land"
        
        let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: location, span: span)
        
        districtMap.setRegion(region, animated: true)
        
        districtMap.addAnnotation(annotation)
        
        mapReady = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollTo(view: String){
        
        UIView.animate(withDuration: 0.4, animations: {
            
            switch view{
            case "view-list":
                let scrollTo = CGRect(x: self.listView.frame.origin.x, y: self.listView.frame.origin.y, width: self.listView.frame.width, height: self.listView.frame.height)
                self.scrollView.scrollRectToVisible(scrollTo, animated: false)
                
            case "view-map":
                let scrollTo = CGRect(x: self.districtMap.frame.origin.x, y: self.districtMap.frame.origin.y, width: self.districtMap.frame.width, height: self.districtMap.frame.height)
                self.scrollView.scrollRectToVisible(scrollTo, animated: false)
            default:
                print("erreur scroll")
                
            }
            
            
        })


    }
    
    @IBAction func switchView(_ sender: Any) {
        
        var switchView: String
        if mapReady {
            switchView = "view-list"
            mapReady = false
            mainButton.setTitle("Afficher la carte de mon quartier", for: .normal)
        }else{
            switchView = "view-map"
            mapReady = true
            mainButton.setTitle("Afficher la liste de mon quartier", for: .normal)
        }
        
        scrollTo(view: switchView)
        
    }
}
