//
//  ConfirmAdressController.swift
//  MyVillage
//
//  Created by Victor Guerinon on 18/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation

class ConfirmAdressViewController: UIViewController {
    
    @IBOutlet var districtMap: MKMapView!
    
    var nomF: String?
    var prenomF: String?
    var mailF: String?
    var numeroF: String?
    var nmFamille: String?
    var adresse: String?
    var ville: String?
    var codePostal: String?
    var mapReady : Bool = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let adresseComplete : String = adresse! + " " + codePostal! + " " + ville!
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(adresseComplete) {(placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location
                else {
                    return
            }
            let localisation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = localisation
            annotation.title = "Ma Maison"
            
            let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
            let region = MKCoordinateRegion(center: localisation, span: span)
            
            self.districtMap.setRegion(region, animated: true)
            self.districtMap.addAnnotation(annotation)
            self.mapReady = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "adresseOK" {
            let nomFoyer = segue.destination as? ConfirmInformationViewController
            let prenomFoyer = segue.destination as? ConfirmInformationViewController
            let mailFoyer = segue.destination as? ConfirmInformationViewController
            let numeroFoyer = segue.destination as? ConfirmInformationViewController
            let nomFamille = segue.destination as? ConfirmInformationViewController
            let adresseFoyer = segue.destination as? ConfirmInformationViewController
            let codePostalFoyer = segue.destination as? ConfirmInformationViewController
            let villeFoyer = segue.destination as? ConfirmInformationViewController
            
            
            nomFoyer?.nomF = nomF
            prenomFoyer?.prenomF = prenomF
            mailFoyer?.mailF = mailF
            numeroFoyer?.numeroF = numeroF
            nomFamille?.nmFamille = nmFamille
            adresseFoyer?.adresseF = adresse
            codePostalFoyer?.codePostalF = codePostal
            villeFoyer?.villeF = ville
         
            
            
        } else {
            let nomFoyer = segue.destination as? CreateAdressViewController
            let prenomFoyer = segue.destination as? CreateAdressViewController
            let mailFoyer = segue.destination as? CreateAdressViewController
            let numeroFoyer = segue.destination as? CreateAdressViewController
            let nomFamille = segue.destination as? CreateAdressViewController
            let adresseFoyer = segue.destination as? CreateAdressViewController
            let codePostalFoyer = segue.destination as? CreateAdressViewController
            let villeFoyer = segue.destination as? CreateAdressViewController
            
            nomFoyer?.nomF = nomF
            prenomFoyer?.prenomF = prenomF
            mailFoyer?.mailF = mailF
            numeroFoyer?.numeroF = numeroF
            nomFamille?.nmFamille = nmFamille
            adresseFoyer?.adresse = adresse
            codePostalFoyer?.codePostal = codePostal
            villeFoyer?.ville = ville
        }
    }
}
