//
//  AlertEnums.swift
//  MyVillage
//
//  Created by Victor Guerinon on 07/09/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation

public enum TypeAlerte : String {
    case CAMBRIOLAGE
    case INCENDIE
    case DETRESSE
}

public enum TypeDanger : String {
    case SOUPCON
    case DANGER
}

public enum TypeLocation : String {
    case HOME
    case DISTRICT
}
