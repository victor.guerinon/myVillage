//
//  ViewLocationAlert.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 22/03/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class LocationViewController: UIViewController {
    
    var typeAlert: TypeAlerte!
    var typeDanger: TypeDanger!
    var locationAlert: TypeLocation!
    var callService: Bool = false
  
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        switch segue.identifier {
        case "locateHomeSegue"?:
            locationAlert = .HOME
            break
        case "locateDistrictSegue"?:
            locationAlert = .DISTRICT
        default:
            break
        }
        
        let levelAlertVC = segue.destination as? ConfirmAlertViewController
        let typeAlertVC = segue.destination as? ConfirmAlertViewController
        let locationAlertVC = segue.destination as? ConfirmAlertViewController
        
        locationAlertVC?.locationAlert = locationAlert
        levelAlertVC?.typeDanger = typeDanger
        typeAlertVC?.typeAlert = typeAlert
        
        
    }

    
   
    
    
    
}

