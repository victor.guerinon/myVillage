//
//  CreateAdressController.swift
//  MyVillage
//
//  Created by Victor Guerinon on 17/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit

class CreateAdressViewController: UIViewController {
    @IBOutlet var adresseF: UITextField!
    @IBOutlet var codePostalF: UITextField!
    @IBOutlet var villeF: UITextField!
    
    var nomF: String?
    var prenomF: String?
    var mailF: String?
    var numeroF: String?
    var nmFamille : String?
    var adresse : String?
    var ville: String?
    var codePostal : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nomFoyer = segue.destination as? ConfirmAdressViewController
        let prenomFoyer = segue.destination as? ConfirmAdressViewController
        let mailFoyer = segue.destination as? ConfirmAdressViewController
        let numeroFoyer = segue.destination as? ConfirmAdressViewController
        let nomFamille = segue.destination as? ConfirmAdressViewController
        let adresseFoyer = segue.destination as? ConfirmAdressViewController
        let codePostalFoyer = segue.destination as? ConfirmAdressViewController
        let villeFoyer = segue.destination as? ConfirmAdressViewController
        
        nomFoyer?.nomF = nomF
        prenomFoyer?.prenomF = prenomF
        mailFoyer?.mailF = mailF
        numeroFoyer?.numeroF = numeroF
        nomFamille?.nmFamille = nmFamille
        adresseFoyer?.adresse = adresseF.text
        codePostalFoyer?.codePostal = codePostalF.text
        villeFoyer?.ville = villeF.text
        
    }
    
}


