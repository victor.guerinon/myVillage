//
//  Context.swift
//  MyVillage
//
//  Created by Victor Guerinon on 14/09/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation

public enum CtxtSubDistrict {
    case SIGNNIN
    case NORMAL
}

