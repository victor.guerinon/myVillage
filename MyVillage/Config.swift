//
//  Config.swift
//  MyVillage
//
//  Created by Damien Hervé on 28/09/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import MyVillageAPI
import Alamofire

struct Config {
    
    #if VALID
    static let MYVILLAGE_BASEPATH = "https://valid-digital.covea.fr/myvillage"
    #elseif PROD
    static let MYVILLAGE_BASEPATH = "https://digital.covea.fr/myvillage"
    #endif
    
    public static func setupAPI() {
        MyVillageAPIAPI.basePath = MYVILLAGE_BASEPATH
        print(MyVillageAPIAPI.basePath)
    }
    
    static func setAuthentication(_ credentials : Credentials) {
        guard let idFoyer = credentials.idFoyer, let password = credentials.password else {
            return
        }
        //MyVillageAPIAPI.credential = URLCredential(user: idFoyer, password: password, persistence: URLCredential.Persistence.none)
        let creds = Request.authorizationHeader(user: idFoyer, password: password)
        if let key = creds?.key, let value = creds?.value {
            let headers = [String:String](dictionaryLiteral: (key, value))
            MyVillageAPIAPI.customHeaders = headers
            print(MyVillageAPIAPI.customHeaders)
        }
    }
}
