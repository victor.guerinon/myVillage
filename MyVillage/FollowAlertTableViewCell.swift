//
//  FollowAlertTableViewCell.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 24/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class FollowAlertTableViewCell: UITableViewCell{
    
    @IBOutlet var hourReact: UILabel!
    @IBOutlet var iconReactAlert: UIImageView!
    @IBOutlet var nameNeighbor: UILabel!
    @IBOutlet var textReact: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
