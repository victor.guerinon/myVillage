//
//  UserInformationsController.swift
//  MyVillage
//
//  Created by Victor Guerinon on 17/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import Foundation
import UIKit

class UserInformationsViewController: UIViewController {
    @IBOutlet var nomF: UITextField!
    @IBOutlet var prenomF: UITextField!
    @IBOutlet var mailF: UITextField!
    @IBOutlet var numeroF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nomFoyer = segue.destination as? CreateHomeViewController
        let prenomFoyer = segue.destination as? CreateHomeViewController
        let mailFoyer = segue.destination as? CreateHomeViewController
        let numeroFoyer = segue.destination as? CreateHomeViewController
        
        nomFoyer?.nomF = nomF.text
        prenomFoyer?.prenomF = prenomF.text
        mailFoyer?.mailF = mailF.text
        numeroFoyer?.numeroF = numeroF.text
    }
    
}
