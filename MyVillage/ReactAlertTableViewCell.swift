//
//  ReactAlertTableViewCell.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 25/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class ReactAlertTableViewCell: UITableViewCell{

    @IBOutlet var textReact: UILabel!
    @IBOutlet var iconReact: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
