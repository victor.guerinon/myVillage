//
//  FollowAlertViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 23/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class FollowAlertViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var timelineReactAlert: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        timelineReactAlert.backgroundColor = UIColor(colorLiteralRed: 0/255, green: 0/255, blue: 0/255, alpha: 0)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //a modifier selon longueur de la timeline récupérer
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timeline", for: indexPath) as! FollowAlertTableViewCell
        
        cell.backgroundColor = UIColor(colorLiteralRed: 0/255, green: 0/255, blue: 0/255, alpha: 0)
        
        return cell
        
    }
    
}
