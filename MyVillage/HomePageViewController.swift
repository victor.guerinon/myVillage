//
//  ViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 21/03/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit
import MyVillageAPI

class HomePageViewController: UIViewController {

        override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
            
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "incendieSegue"?:
            let destinationVC = segue.destination as! UINavigationController
            let detailVC = destinationVC.viewControllers.first as! TypeAlertViewController
            detailVC.typeAlert = .INCENDIE
        case "detresseSegue"?:
            let destinationVC = segue.destination as! UINavigationController
            let detailVC = destinationVC.viewControllers.first as! TypeAlertViewController
            detailVC.typeAlert = .DETRESSE
        case "cambriolageSegue"?:
            let destinationVC = segue.destination as! UINavigationController
            let detailVC = destinationVC.viewControllers.first as! TypeAlertViewController
            detailVC.typeAlert = .CAMBRIOLAGE
        case "followAlertSegue"? :
            let destinationVC = segue.destination as! UINavigationController
            let detailVC = destinationVC.viewControllers.first as! NeighborAlertViewController
        case "showDistrictSegue"?:
            let destinationVC = segue.destination as! UINavigationController
            let detailVC = destinationVC.viewControllers.first as! ShowDistrictViewController
        default:
            break
        }
    }
}

