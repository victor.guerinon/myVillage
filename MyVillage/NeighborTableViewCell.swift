//
//  NeighborTableViewCell.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 22/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class NeighborTableViewCell: UITableViewCell{
    
    @IBOutlet var dateAlert: UILabel!
    @IBOutlet var lvlAlert: UILabel!
    @IBOutlet var stateAlert: UILabel!
    @IBOutlet var adressAlert: UILabel!
    
    @IBOutlet var distance: UILabel!
    
    @IBOutlet var nameImpacte: UILabel!
    @IBOutlet var nameEmetteur: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
