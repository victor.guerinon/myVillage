//
//  ReactAlertViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 24/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class ReactAlertViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    // Création de la liste des réaction complete
    var reactUserList: [ReactAlert] = []
    
    //A modifier selon le type de l'alerte
    var typeAlert = "thief alert"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Liste créer pour un utilisateur AUTRE a modifier selon le type d'utilisateur
        reactUserList += [.obsOK, .obsDANGER, .callPOLICE, .obsPOLICE, .obsRAS]
       
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return reactUserList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reactCell = reactUserList[reactUserList.index(before: (indexPath.row + 1))]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reaction", for: indexPath) as! ReactAlertTableViewCell
            
        return createReact(react: reactCell, cellReact: cell)
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name:"FollowAlert", bundle:nil)
        let followAlert = storyboard.instantiateViewController(withIdentifier: "follow-alert") as! FollowAlertViewController
        
        self.navigationController?.pushViewController(followAlert, animated: true)
    }

    //Creation de la cellule de réaction
    func createReact(react: ReactAlert, cellReact: ReactAlertTableViewCell) -> UITableViewCell{
        
        switch react {
        case .obsOK:
            cellReact.textReact.text = "J'ai bien reçu l'alerte, merci"
            cellReact.iconReact.image = #imageLiteral(resourceName: "obs-ok")
            break
        case .obsDANGER:
            cellReact.textReact.text = "Je confirme, le danger est réel !"
            cellReact.iconReact.image = #imageLiteral(resourceName: "obs-alert-avere")
            break
        case .actDANGER:
            cellReact.textReact.text = "Il s'agit d'un danger avéré !"
            cellReact.iconReact.image = #imageLiteral(resourceName: "act-alert")
            break
        case .obsRAS:
            cellReact.textReact.text = "Je n'ai rien vu d'alarmant"
            cellReact.iconReact.image = #imageLiteral(resourceName: "obs-ras")
            break
        case .obsPOLICE:
            switch typeAlert {
            case "fire alert":
                cellReact.textReact.text = "Les pompiers sont arrivés sur place."
                cellReact.iconReact.image = #imageLiteral(resourceName: "valid-fireman")
                break
            case "thief alert":
                cellReact.textReact.text = "La police est arrivée sur place."
                cellReact.iconReact.image = #imageLiteral(resourceName: "valid-policeman")
                break
            case "people alert":
                cellReact.textReact.text = "Les secours sont arrivés sur place."
                cellReact.iconReact.image = #imageLiteral(resourceName: "valid-samu")
                break
            default:
                print("error type alert")
                break
            }
        case .callPOLICE:
            switch typeAlert {
            case "fire alert":
                cellReact.textReact.text = "J'ai prévenu les pompiers."
                cellReact.iconReact.image = #imageLiteral(resourceName: "obs-call-fireman")
                break
            case "thief alert":
                cellReact.textReact.text = "J'ai prévenu la police."
                cellReact.iconReact.image = #imageLiteral(resourceName: "obs-call-policeman")
                break
            case "people alert":
                cellReact.textReact.text = "J'ai prévenu les secours."
                cellReact.iconReact.image = #imageLiteral(resourceName: "obs-call-samu")
                break
            default:
                print("error type alert")
                break
            }
        default:
            print("error reaction list")
            break
        }
        
        return cellReact
    }

    
    enum ReactAlert{
        case obsOK, obsDANGER, actDANGER, obsRAS, obsPOLICE, callPOLICE
    }
    
}
