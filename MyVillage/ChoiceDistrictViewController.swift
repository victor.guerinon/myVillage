//
//  ChoiceDistrictViewController.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 10/08/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class ChoiceDistrictViewController: UIViewController{
    
    @IBOutlet var btnChoiceDistrict: UIButton!
    @IBOutlet var btnAllTown: UIButton!
    @IBOutlet var btnAbonnement: UIButton!
    
    @IBOutlet var viewAllTown: UIView!
    @IBOutlet var viewChoiceDistrict: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    var selectAllTown: Bool = true
    var context : CtxtSubDistrict!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        optionSelected()
        
        let currentWidthScreen = UIScreen.main.bounds.width
        
        scrollView.contentSize = CGSize(width: 2 * currentWidthScreen, height: 300)
        
        scrollView.isScrollEnabled = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func optionSelected(){
        
        let colorSelected = UIColor(colorLiteralRed: 95/255, green: 164/255, blue: 198/255, alpha: 1)
        let basicColor = UIColor(colorLiteralRed: 150/255, green: 156/255, blue: 159/255, alpha: 1)
        
        if selectAllTown{
            btnAllTown.backgroundColor = colorSelected
            btnChoiceDistrict.backgroundColor = basicColor
        }else{
            btnAllTown.backgroundColor = basicColor
            btnChoiceDistrict.backgroundColor = colorSelected
        }
    }
    
    
    func scrollTo(view: UIView){
        
        UIView.animate(withDuration: 0.4, animations: {
            
            switch view{
            case self.viewAllTown:
                let scrollTo = CGRect(x: self.viewAllTown.frame.origin.x, y: self.viewAllTown.frame.origin.y, width: self.viewAllTown.frame.width, height: self.viewAllTown.frame.height)
                self.scrollView.scrollRectToVisible(scrollTo, animated: false)
                
            case self.viewChoiceDistrict:
                let scrollTo = CGRect(x: self.viewChoiceDistrict.frame.origin.x, y: self.viewChoiceDistrict.frame.origin.y, width: self.viewChoiceDistrict.frame.width, height: self.viewChoiceDistrict.frame.height)
                self.scrollView.scrollRectToVisible(scrollTo, animated: false)
            default:
                print("erreur scroll")
            }
        })
    }
    

    @IBAction func onClickAllTown(_ sender: Any) {
        selectAllTown = true
        optionSelected()
        scrollTo(view: viewAllTown)
    }
    
    @IBAction func onClickChoiceDistrict(_ sender: Any) {
        selectAllTown = false
        optionSelected()
        scrollTo(view: viewChoiceDistrict)
    }
    
    
    @IBAction func clickButtonAbo(sender: UIButton) {
        print(context)
        switch context! {
        case .SIGNNIN:
            let storyboard = UIStoryboard(name: "SignIn", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ConfirmSignIn") as UIViewController
            present(vc, animated: true, completion: nil)
            
        case .NORMAL:
            break
        }
    }
    
    
    
}
