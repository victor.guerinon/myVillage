//
//  ViewConfirmAlert.swift
//  MyVillage
//
//  Created by Léo Ganachaud on 30/03/2017.
//  Copyright © 2017 digitalFactory. All rights reserved.
//

import UIKit

class ConfirmAlertViewController: UIViewController {
    
    
    @IBOutlet var typeAlertItem: UIImageView!
    @IBOutlet var levelAlertItem: UIImageView!
    @IBOutlet var locationItem: UIImageView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var levelAlertLabel: UILabel!
    @IBOutlet var typeAlertLabel: UILabel!
    
    var typeAlert: TypeAlerte!
    var typeDanger: TypeDanger!
    var locationAlert: TypeLocation!
    
    // Créer fonction pour lancer alerte
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch typeDanger! {
        case .DANGER:
            levelAlertItem.image = #imageLiteral(resourceName: "alarm-alert")
            levelAlertLabel.text = "Danger avéré"
        case .SOUPCON:
            levelAlertItem.image = #imageLiteral(resourceName: "suspicious-alert")
            levelAlertLabel.text = "Soupçon"
        }

        
        switch typeAlert! {
        case .INCENDIE:
            typeAlertItem.image = #imageLiteral(resourceName: "fire-alert")
            typeAlertLabel.text = "Incendie"
        case .CAMBRIOLAGE:
            typeAlertItem.image = #imageLiteral(resourceName: "thief-alert")
            typeAlertLabel.text = "Cambriolage"
        case .DETRESSE:
            typeAlertItem.image = #imageLiteral(resourceName: "people-alert")
            typeAlertLabel.text = "Personne en détresse"
        }
        
        switch locationAlert! {
        case .HOME:
            locationItem.image = #imageLiteral(resourceName: "home-alert")
            locationLabel.text = "Chez moi"
        case .DISTRICT:
            locationItem.image = #imageLiteral(resourceName: "town-alert")
            locationLabel.text = "Dans mon quartier"
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func msgCancelAlert() {
        let alert = UIAlertController(title: "Annulation de votre déclaration", message: "Voulez-vous vraiment annuler votre déclaration ?", preferredStyle: .alert)
        let no = UIAlertAction(title: "Non", style: UIAlertActionStyle.cancel, handler: nil)
        let yes = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(UIAlertAction) -> Void in
            
            let storyboard = UIStoryboard(name: "AlertProcess", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "HomePage") as! HomePageViewController
            
            self.navigationController?.show(controller, sender: self)
            
        })
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
        
    }
}

