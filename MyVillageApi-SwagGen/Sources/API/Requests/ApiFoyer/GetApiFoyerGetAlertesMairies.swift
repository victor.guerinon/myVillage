//
// Generated by SwagGen
// https://github.com/yonaskolb/SwagGen
//

import Foundation
import JSONUtilities

extension API.ApiFoyer {

    /** Permet de récupérer l'ensemble des alertes mairies */
    public enum GetApiFoyerGetAlertesMairies {

        public static let service = APIService<Response>(id: "getApiFoyerGetAlertesMairies", tag: "Api Foyer", method: "GET", path: "/apiFoyer/getAlertesMairies", hasBody: false)

        public final class Request: APIRequest<Response> {

            public struct Options {

                public var idFoyer: String

                public init(idFoyer: String) {
                    self.idFoyer = idFoyer
                }
            }

            public var options: Options

            public init(options: Options) {
                self.options = options
                super.init(service: GetApiFoyerGetAlertesMairies.service)
            }

            /// convenience initialiser so an Option doesn't have to be created
            public convenience init(idFoyer: String) {
                let options = Options(idFoyer: idFoyer)
                self.init(options: options)
            }

            public override var parameters: [String: Any] {
                var params: JSONDictionary = [:]
                params["idFoyer"] = options.idFoyer
                return params
            }
        }

        public enum Response: APIResponseValue, CustomStringConvertible, CustomDebugStringConvertible {
            public typealias SuccessType = [AlerteMairie]

            /** Succes */
            case status200([AlerteMairie])

            public var success: [AlerteMairie]? {
                switch self {
                case .status200(let response): return response
                }
            }

            public var response: Any {
                switch self {
                case .status200(let response): return response
                }
            }

            public var statusCode: Int {
                switch self {
                case .status200: return 200
                }
            }

            public var successful: Bool {
                switch self {
                case .status200: return true
                }
            }

            public init(statusCode: Int, data: Data) throws {
                switch statusCode {
                case 200: self = try .status200(JSONDecoder.decode(data: data))
                default: throw APIError.unexpectedStatusCode(statusCode: statusCode, data: data)
                }
            }

            public var description: String {
                return "\(statusCode) \(successful ? "success" : "failure")"
            }

            public var debugDescription: String {
                var string = description
                let responseString = "\(response)"
                if responseString != "()" {
                    string += "\n\(responseString)"
                }
                return string
            }
        }
    }
}
