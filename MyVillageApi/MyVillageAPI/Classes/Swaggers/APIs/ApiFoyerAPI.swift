//
// ApiFoyerAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Alamofire

open class ApiFoyerAPI: APIBase {
    /**
     Ajout d'un device
     
     - parameter idFoyer: (query)  
     - parameter UUID: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerAjouterDevicePost(idFoyer: String, UUID: String, completion: @escaping ((_ error: Error?) -> Void)) {
        apiFoyerAjouterDevicePostWithRequestBuilder(idFoyer: idFoyer, UUID: UUID).execute { (response, error) -> Void in
            completion(error);
        }
    }


    /**
     Ajout d'un device
     - POST /apiFoyer/ajouterDevice
     - Permet d'ajouter un device et de le lié à un user
     - BASIC:
       - type: basic
       - name: Basic
     
     - parameter idFoyer: (query)  
     - parameter UUID: (query)  

     - returns: RequestBuilder<Void> 
     */
    open class func apiFoyerAjouterDevicePostWithRequestBuilder(idFoyer: String, UUID: String) -> RequestBuilder<Void> {
        let path = "/apiFoyer/ajouterDevice"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idFoyer": idFoyer,
            "UUID": UUID
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<Void>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Ajout d'un foyer
     
     - parameter nom: (query)  
     - parameter latitude: (query)  
     - parameter longitude: (query)  
     - parameter numeroTelephone: (query)  
     - parameter rue: (query)  
     - parameter codePostal: (query)  
     - parameter commune: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerAjouterPost(nom: String, latitude: String, longitude: String, numeroTelephone: String, rue: String, codePostal: String, commune: String, completion: @escaping ((_ data: Credentials?,_ error: Error?) -> Void)) {
        apiFoyerAjouterPostWithRequestBuilder(nom: nom, latitude: latitude, longitude: longitude, numeroTelephone: numeroTelephone, rue: rue, codePostal: codePostal, commune: commune).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Ajout d'un foyer
     - POST /apiFoyer/ajouter
     - Permet d'ajouter un foyer
     - examples: [{contentType=application/json, example={
  "password" : "aeiou",
  "idFoyer" : "aeiou"
}}]
     
     - parameter nom: (query)  
     - parameter latitude: (query)  
     - parameter longitude: (query)  
     - parameter numeroTelephone: (query)  
     - parameter rue: (query)  
     - parameter codePostal: (query)  
     - parameter commune: (query)  

     - returns: RequestBuilder<Credentials> 
     */
    open class func apiFoyerAjouterPostWithRequestBuilder(nom: String, latitude: String, longitude: String, numeroTelephone: String, rue: String, codePostal: String, commune: String) -> RequestBuilder<Credentials> {
        let path = "/apiFoyer/ajouter"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "nom": nom,
            "latitude": latitude,
            "longitude": longitude,
            "numeroTelephone": numeroTelephone,
            "rue": rue,
            "codePostal": codePostal,
            "commune": commune
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<Credentials>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Connecte l'utilisateur
     
     - parameter idFoyer: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerConnectePost(idFoyer: String, completion: @escaping ((_ error: Error?) -> Void)) {
        apiFoyerConnectePostWithRequestBuilder(idFoyer: idFoyer).execute { (response, error) -> Void in
            completion(error);
        }
    }


    /**
     Connecte l'utilisateur
     - POST /apiFoyer/connecte
     - Permet de savoir quand  l'utilisateur s’est connecté
     - BASIC:
       - type: basic
       - name: Basic
     
     - parameter idFoyer: (query)  

     - returns: RequestBuilder<Void> 
     */
    open class func apiFoyerConnectePostWithRequestBuilder(idFoyer: String) -> RequestBuilder<Void> {
        let path = "/apiFoyer/connecte"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idFoyer": idFoyer
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<Void>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Récupère les abonnements quartiers
     
     - parameter idFoyer: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerGetAbonnementsQuartiersGet(idFoyer: String, completion: @escaping ((_ data: [Quartier]?,_ error: Error?) -> Void)) {
        apiFoyerGetAbonnementsQuartiersGetWithRequestBuilder(idFoyer: idFoyer).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Récupère les abonnements quartiers
     - GET /apiFoyer/getAbonnementsQuartiers
     - Récupère l'ensemble des abonnements quartiers du user
     - BASIC:
       - type: basic
       - name: Basic
     - examples: [{contentType=application/json, example=[ {
  "alertes" : [ {
    "foyerEmetteur" : {
      "identifiantUnique" : "aeiou",
      "abonnementQuartier" : [ "" ],
      "utilisateurs" : [ {
        "id" : "aeiou",
        "nom" : "aeiou",
        "prenom" : "aeiou"
      } ],
      "lastUpdated" : "2000-01-23T04:56:07.000+00:00",
      "dateCreated" : "2000-01-23T04:56:07.000+00:00",
      "immeuble" : {
        "latitude" : "aeiou",
        "adresse" : "aeiou",
        "id" : "",
        "longitude" : "aeiou"
      },
      "id" : "aeiou",
      "nom" : "aeiou",
      "numeroTelephone" : "aeiou",
      "uuid" : [ "aeiou" ]
    },
    "dateCreated" : "2000-01-23T04:56:07.000+00:00",
    "etatCourant" : "aeiou",
    "typeAlerte" : "aeiou",
    "evenements" : [ {
      "dateCreated" : "2000-01-23T04:56:07.000+00:00",
      "typeEvenement" : "aeiou",
      "nomFoyerEmetteur" : "aeiou"
    } ],
    "id" : "aeiou",
    "foyerImpacte" : ""
  } ],
  "marie" : "aeiou",
  "id" : "aeiou",
  "nom" : "aeiou",
  "tous" : true
} ]}]
     
     - parameter idFoyer: (query)  

     - returns: RequestBuilder<[Quartier]> 
     */
    open class func apiFoyerGetAbonnementsQuartiersGetWithRequestBuilder(idFoyer: String) -> RequestBuilder<[Quartier]> {
        let path = "/apiFoyer/getAbonnementsQuartiers"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idFoyer": idFoyer
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<[Quartier]>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Récupère les alertes mairies
     
     - parameter idFoyer: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerGetAlertesMairiesGet(idFoyer: String, completion: @escaping ((_ data: [AlerteMairie]?,_ error: Error?) -> Void)) {
        apiFoyerGetAlertesMairiesGetWithRequestBuilder(idFoyer: idFoyer).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Récupère les alertes mairies
     - GET /apiFoyer/getAlertesMairies
     - Permet de récupérer l'ensemble des alertes mairies
     - BASIC:
       - type: basic
       - name: Basic
     - examples: [{contentType=application/json, example=[ {
  "fateFinAlerte" : "2000-01-23T04:56:07.000+00:00",
  "dateDebutAlerte" : "2000-01-23T04:56:07.000+00:00",
  "quartiersAlertes" : [ {
    "alertes" : [ {
      "foyerEmetteur" : {
        "identifiantUnique" : "aeiou",
        "abonnementQuartier" : [ "" ],
        "utilisateurs" : [ {
          "id" : "aeiou",
          "nom" : "aeiou",
          "prenom" : "aeiou"
        } ],
        "lastUpdated" : "2000-01-23T04:56:07.000+00:00",
        "dateCreated" : "2000-01-23T04:56:07.000+00:00",
        "immeuble" : {
          "latitude" : "aeiou",
          "adresse" : "aeiou",
          "id" : "",
          "longitude" : "aeiou"
        },
        "id" : "aeiou",
        "nom" : "aeiou",
        "numeroTelephone" : "aeiou",
        "uuid" : [ "aeiou" ]
      },
      "dateCreated" : "2000-01-23T04:56:07.000+00:00",
      "etatCourant" : "aeiou",
      "typeAlerte" : "aeiou",
      "evenements" : [ {
        "dateCreated" : "2000-01-23T04:56:07.000+00:00",
        "typeEvenement" : "aeiou",
        "nomFoyerEmetteur" : "aeiou"
      } ],
      "id" : "aeiou",
      "foyerImpacte" : ""
    } ],
    "marie" : "aeiou",
    "id" : "aeiou",
    "nom" : "aeiou",
    "tous" : true
  } ],
  "texteAlerte" : "aeiou",
  "id" : "aeiou"
} ]}]
     
     - parameter idFoyer: (query)  

     - returns: RequestBuilder<[AlerteMairie]> 
     */
    open class func apiFoyerGetAlertesMairiesGetWithRequestBuilder(idFoyer: String) -> RequestBuilder<[AlerteMairie]> {
        let path = "/apiFoyer/getAlertesMairies"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idFoyer": idFoyer
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<[AlerteMairie]>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Récuperation d'un foyer
     
     - parameter idFoyer: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerGetFoyerGet(idFoyer: String, completion: @escaping ((_ data: Foyer?,_ error: Error?) -> Void)) {
        apiFoyerGetFoyerGetWithRequestBuilder(idFoyer: idFoyer).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Récuperation d'un foyer
     - GET /apiFoyer/getFoyer
     - Permet de récuperer le foyer correpondant à l'identifiant
     - BASIC:
       - type: basic
       - name: Basic
     - examples: [{contentType=application/json, example={
  "identifiantUnique" : "aeiou",
  "abonnementQuartier" : [ {
    "alertes" : [ {
      "foyerEmetteur" : "",
      "dateCreated" : "2000-01-23T04:56:07.000+00:00",
      "etatCourant" : "aeiou",
      "typeAlerte" : "aeiou",
      "evenements" : [ {
        "dateCreated" : "2000-01-23T04:56:07.000+00:00",
        "typeEvenement" : "aeiou",
        "nomFoyerEmetteur" : "aeiou"
      } ],
      "id" : "aeiou",
      "foyerImpacte" : ""
    } ],
    "marie" : "aeiou",
    "id" : "aeiou",
    "nom" : "aeiou",
    "tous" : true
  } ],
  "utilisateurs" : [ {
    "id" : "aeiou",
    "nom" : "aeiou",
    "prenom" : "aeiou"
  } ],
  "lastUpdated" : "2000-01-23T04:56:07.000+00:00",
  "dateCreated" : "2000-01-23T04:56:07.000+00:00",
  "immeuble" : {
    "latitude" : "aeiou",
    "adresse" : "aeiou",
    "id" : "",
    "longitude" : "aeiou"
  },
  "id" : "aeiou",
  "nom" : "aeiou",
  "numeroTelephone" : "aeiou",
  "uuid" : [ "aeiou" ]
}}]
     
     - parameter idFoyer: (query)  

     - returns: RequestBuilder<Foyer> 
     */
    open class func apiFoyerGetFoyerGetWithRequestBuilder(idFoyer: String) -> RequestBuilder<Foyer> {
        
        let path = "/apiFoyer/getFoyer"
        let URLString = MyVillageAPIAPI.basePath + path
        print(URLString)

        let nillableParameters: [String:Any?] = [
            "idFoyer": idFoyer
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<Foyer>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Recupere les foyers présent dans idImmeuble
     
     - parameter idImmeuble: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerGetFoyersGet(idImmeuble: String, completion: @escaping ((_ data: [GetFoyerResponse]?,_ error: Error?) -> Void)) {
        apiFoyerGetFoyersGetWithRequestBuilder(idImmeuble: idImmeuble).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Recupere les foyers présent dans idImmeuble
     - GET /apiFoyer/getFoyers
     - Liste de tous les foyers présent dans un immeuble
     - BASIC:
       - type: basic
       - name: Basic
     - examples: [{contentType=application/json, example=[ {
  "foyers" : [ {
    "identifiantUnique" : "aeiou",
    "abonnementQuartier" : [ {
      "alertes" : [ {
        "foyerEmetteur" : "",
        "dateCreated" : "2000-01-23T04:56:07.000+00:00",
        "etatCourant" : "aeiou",
        "typeAlerte" : "aeiou",
        "evenements" : [ {
          "dateCreated" : "2000-01-23T04:56:07.000+00:00",
          "typeEvenement" : "aeiou",
          "nomFoyerEmetteur" : "aeiou"
        } ],
        "id" : "aeiou",
        "foyerImpacte" : ""
      } ],
      "marie" : "aeiou",
      "id" : "aeiou",
      "nom" : "aeiou",
      "tous" : true
    } ],
    "utilisateurs" : [ {
      "id" : "aeiou",
      "nom" : "aeiou",
      "prenom" : "aeiou"
    } ],
    "lastUpdated" : "2000-01-23T04:56:07.000+00:00",
    "dateCreated" : "2000-01-23T04:56:07.000+00:00",
    "immeuble" : {
      "latitude" : "aeiou",
      "adresse" : "aeiou",
      "id" : "",
      "longitude" : "aeiou"
    },
    "id" : "aeiou",
    "nom" : "aeiou",
    "numeroTelephone" : "aeiou",
    "uuid" : [ "aeiou" ]
  } ],
  "immeuble" : ""
} ]}]
     
     - parameter idImmeuble: (query)  

     - returns: RequestBuilder<[GetFoyerResponse]> 
     */
    open class func apiFoyerGetFoyersGetWithRequestBuilder(idImmeuble: String) -> RequestBuilder<[GetFoyerResponse]> {
        let path = "/apiFoyer/getFoyers"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idImmeuble": idImmeuble
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<[GetFoyerResponse]>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Récupère les immeubles
     
     - parameter latitude: (query)  
     - parameter longitude: (query)  
     - parameter delta: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerGetImmeublesGet(latitude: String, longitude: String, delta: Int32, completion: @escaping ((_ data: [Immeuble]?,_ error: Error?) -> Void)) {
        apiFoyerGetImmeublesGetWithRequestBuilder(latitude: latitude, longitude: longitude, delta: delta).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Récupère les immeubles
     - GET /apiFoyer/getImmeubles
     - Permet de récupérer l'ensemble des immeubles présent dans le delta
     - BASIC:
       - type: basic
       - name: Basic
     - examples: [{contentType=application/json, example=[ {
  "latitude" : "aeiou",
  "adresse" : "aeiou",
  "id" : "",
  "longitude" : "aeiou"
} ]}]
     
     - parameter latitude: (query)  
     - parameter longitude: (query)  
     - parameter delta: (query)  

     - returns: RequestBuilder<[Immeuble]> 
     */
    open class func apiFoyerGetImmeublesGetWithRequestBuilder(latitude: String, longitude: String, delta: Int32) -> RequestBuilder<[Immeuble]> {
        let path = "/apiFoyer/getImmeubles"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "latitude": latitude,
            "longitude": longitude,
            "delta": delta.encodeToJSON()
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<[Immeuble]>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Récupère tous les voisins
     
     - parameter idFoyer: (query)  
     - parameter delta: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerGetVoisinageGet(idFoyer: String, delta: Int32, completion: @escaping ((_ data: [Foyer]?,_ error: Error?) -> Void)) {
        apiFoyerGetVoisinageGetWithRequestBuilder(idFoyer: idFoyer, delta: delta).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }


    /**
     Récupère tous les voisins
     - GET /apiFoyer/getVoisinage
     - Permet de récupérer l'ensemble des voisins du user en fonction du delta
     - BASIC:
       - type: basic
       - name: Basic
     - examples: [{contentType=application/json, example=[ {
  "identifiantUnique" : "aeiou",
  "abonnementQuartier" : [ {
    "alertes" : [ {
      "foyerEmetteur" : "",
      "dateCreated" : "2000-01-23T04:56:07.000+00:00",
      "etatCourant" : "aeiou",
      "typeAlerte" : "aeiou",
      "evenements" : [ {
        "dateCreated" : "2000-01-23T04:56:07.000+00:00",
        "typeEvenement" : "aeiou",
        "nomFoyerEmetteur" : "aeiou"
      } ],
      "id" : "aeiou",
      "foyerImpacte" : ""
    } ],
    "marie" : "aeiou",
    "id" : "aeiou",
    "nom" : "aeiou",
    "tous" : true
  } ],
  "utilisateurs" : [ {
    "id" : "aeiou",
    "nom" : "aeiou",
    "prenom" : "aeiou"
  } ],
  "lastUpdated" : "2000-01-23T04:56:07.000+00:00",
  "dateCreated" : "2000-01-23T04:56:07.000+00:00",
  "immeuble" : {
    "latitude" : "aeiou",
    "adresse" : "aeiou",
    "id" : "",
    "longitude" : "aeiou"
  },
  "id" : "aeiou",
  "nom" : "aeiou",
  "numeroTelephone" : "aeiou",
  "uuid" : [ "aeiou" ]
} ]}]
     
     - parameter idFoyer: (query)  
     - parameter delta: (query)  

     - returns: RequestBuilder<[Foyer]> 
     */
    open class func apiFoyerGetVoisinageGetWithRequestBuilder(idFoyer: String, delta: Int32) -> RequestBuilder<[Foyer]> {
        let path = "/apiFoyer/getVoisinage"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idFoyer": idFoyer,
            "delta": delta.encodeToJSON()
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<[Foyer]>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

    /**
     Update abonnements quatiers
     
     - parameter idsQuartier: (query)  
     - parameter idFoyer: (query)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func apiFoyerUpdateAbonnementsQuartiersPost(idsQuartier: String, idFoyer: String, completion: @escaping ((_ error: Error?) -> Void)) {
        apiFoyerUpdateAbonnementsQuartiersPostWithRequestBuilder(idsQuartier: idsQuartier, idFoyer: idFoyer).execute { (response, error) -> Void in
            completion(error);
        }
    }


    /**
     Update abonnements quatiers
     - POST /apiFoyer/updateAbonnementsQuartiers
     - Permet de modifier les abonnements quartiers utilisateur
     - BASIC:
       - type: basic
       - name: Basic
     
     - parameter idsQuartier: (query)  
     - parameter idFoyer: (query)  

     - returns: RequestBuilder<Void> 
     */
    open class func apiFoyerUpdateAbonnementsQuartiersPostWithRequestBuilder(idsQuartier: String, idFoyer: String) -> RequestBuilder<Void> {
        let path = "/apiFoyer/updateAbonnementsQuartiers"
        let URLString = MyVillageAPIAPI.basePath + path

        let nillableParameters: [String:Any?] = [
            "idsQuartier": idsQuartier,
            "idFoyer": idFoyer
        ]
 
        let parameters = APIHelper.rejectNil(nillableParameters)
 
        let convertedParameters = APIHelper.convertBoolToString(parameters)
 
        let requestBuilder: RequestBuilder<Void>.Type = MyVillageAPIAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: URLString, parameters: convertedParameters, isBody: false)
    }

}
