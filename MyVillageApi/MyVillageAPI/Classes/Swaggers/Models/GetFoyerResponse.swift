//
// GetFoyerResponse.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


open class GetFoyerResponse: JSONEncodable {
    public var immeuble: Int32?
    public var foyers: [Foyer]?

    public init() {}

    // MARK: JSONEncodable
    open func encodeToJSON() -> Any {
        var nillableDictionary = [String:Any?]()
        nillableDictionary["immeuble"] = self.immeuble?.encodeToJSON()
        nillableDictionary["foyers"] = self.foyers?.encodeToJSON()
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
