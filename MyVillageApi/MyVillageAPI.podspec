Pod::Spec.new do |s|
  s.name = 'MyVillageAPI'
  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.11'
  s.version = '1.0'
  s.source = { :git => 'git@github.com:swagger-api/swagger-mustache.git', :tag => 'v1.0.0' }
  s.authors = 'Swagger Codegen'
  s.license = 'Apache License, Version 2.0'
  s.homepage = 'None'
  s.summary = 'MyVillage API'
  s.description = 'MyVillage API'
  s.source_files = 'MyVillageAPI/Classes/Swaggers/**/*.swift'
  s.dependency 'Alamofire', '~> 4.0'
end
